
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Slave_DA3.c,22 :: 		void interrupt()
;Slave_DA3.c,25 :: 		if(PIR1.RCIF)
	BTFSS      PIR1+0, 5
	GOTO       L_interrupt0
;Slave_DA3.c,27 :: 		while(uart1_data_ready()==0);
L_interrupt1:
	CALL       _UART1_Data_Ready+0
	MOVF       R0+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt2
	GOTO       L_interrupt1
L_interrupt2:
;Slave_DA3.c,28 :: 		if(uart1_data_ready()==1)
	CALL       _UART1_Data_Ready+0
	MOVF       R0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt3
;Slave_DA3.c,31 :: 		Wr_buff(&RxUart1,30,UART1_Read());
	CALL       _UART1_Read+0
	MOVF       R0+0, 0
	MOVWF      FARG_Wr_buff_val+0
	MOVLW      _RxUart1+0
	MOVWF      FARG_Wr_buff_buf_prt+0
	MOVLW      30
	MOVWF      FARG_Wr_buff_buf_size+0
	CALL       _Wr_buff+0
;Slave_DA3.c,32 :: 		}
L_interrupt3:
;Slave_DA3.c,33 :: 		}
L_interrupt0:
;Slave_DA3.c,34 :: 		}
L_end_interrupt:
L__interrupt41:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;Slave_DA3.c,35 :: 		void main()
;Slave_DA3.c,37 :: 		TRISB.B0 =0;
	BCF        TRISB+0, 0
;Slave_DA3.c,38 :: 		TRISB.B4 =0;
	BCF        TRISB+0, 4
;Slave_DA3.c,39 :: 		TRISB.B5 =0;
	BCF        TRISB+0, 5
;Slave_DA3.c,41 :: 		TRISB.B3 =0;                         //Bit RS485
	BCF        TRISB+0, 3
;Slave_DA3.c,42 :: 		PORTB.B3 = 0;
	BCF        PORTB+0, 3
;Slave_DA3.c,45 :: 		UART1_Init(9600);
	MOVLW      129
	MOVWF      SPBRG+0
	BSF        TXSTA+0, 2
	CALL       _UART1_Init+0
;Slave_DA3.c,46 :: 		Delay_ms(100);
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main4:
	DECFSZ     R13+0, 1
	GOTO       L_main4
	DECFSZ     R12+0, 1
	GOTO       L_main4
	DECFSZ     R11+0, 1
	GOTO       L_main4
	NOP
	NOP
;Slave_DA3.c,48 :: 		RCIE_bit = 1;                        // enable interrupt on UART1 receive
	BSF        RCIE_bit+0, BitPos(RCIE_bit+0)
;Slave_DA3.c,49 :: 		TXIE_bit = 0;                        // disable interrupt on UART1 transmit
	BCF        TXIE_bit+0, BitPos(TXIE_bit+0)
;Slave_DA3.c,50 :: 		PEIE_bit = 1;                        // enable peripheral interrupts
	BSF        PEIE_bit+0, BitPos(PEIE_bit+0)
;Slave_DA3.c,51 :: 		GIE_bit = 1;                         // enable all interrupts
	BSF        GIE_bit+0, BitPos(GIE_bit+0)
;Slave_DA3.c,55 :: 		PORTB.RB0 =1;
	BSF        PORTB+0, 0
;Slave_DA3.c,56 :: 		PORTB.RB4 =1;
	BSF        PORTB+0, 4
;Slave_DA3.c,57 :: 		PORTB.RB5 =1;
	BSF        PORTB+0, 5
;Slave_DA3.c,58 :: 		Delay_ms(500);
	MOVLW      13
	MOVWF      R11+0
	MOVLW      175
	MOVWF      R12+0
	MOVLW      182
	MOVWF      R13+0
L_main5:
	DECFSZ     R13+0, 1
	GOTO       L_main5
	DECFSZ     R12+0, 1
	GOTO       L_main5
	DECFSZ     R11+0, 1
	GOTO       L_main5
	NOP
;Slave_DA3.c,59 :: 		PORTB.RB0 =0;
	BCF        PORTB+0, 0
;Slave_DA3.c,60 :: 		PORTB.RB4 =0;
	BCF        PORTB+0, 4
;Slave_DA3.c,61 :: 		PORTB.RB5 =0;
	BCF        PORTB+0, 5
;Slave_DA3.c,62 :: 		while(1)
L_main6:
;Slave_DA3.c,66 :: 		if(Sys_getFrame(&Frame_System_Recive[0],'S','E',9) == 1)
	MOVLW      _Frame_System_Recive+0
	MOVWF      FARG_Sys_getFrame_pData+0
	MOVLW      83
	MOVWF      FARG_Sys_getFrame_CharterStart+0
	MOVLW      69
	MOVWF      FARG_Sys_getFrame_CharterStop+0
	MOVLW      9
	MOVWF      FARG_Sys_getFrame_len+0
	CALL       _Sys_getFrame+0
	MOVF       R0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main8
;Slave_DA3.c,69 :: 		if(Frame_System_Recive[1] == '0' && Frame_System_Recive[2] == 'D')
	MOVF       _Frame_System_Recive+1, 0
	XORLW      48
	BTFSS      STATUS+0, 2
	GOTO       L_main11
	MOVF       _Frame_System_Recive+2, 0
	XORLW      68
	BTFSS      STATUS+0, 2
	GOTO       L_main11
L__main37:
;Slave_DA3.c,72 :: 		if(Frame_System_Recive[5] == '1'){
	MOVF       _Frame_System_Recive+5, 0
	XORLW      49
	BTFSS      STATUS+0, 2
	GOTO       L_main12
;Slave_DA3.c,73 :: 		PORTB.RB0 =1;
	BSF        PORTB+0, 0
;Slave_DA3.c,74 :: 		}
L_main12:
;Slave_DA3.c,75 :: 		if(Frame_System_Recive[5] == '0'){
	MOVF       _Frame_System_Recive+5, 0
	XORLW      48
	BTFSS      STATUS+0, 2
	GOTO       L_main13
;Slave_DA3.c,76 :: 		PORTB.RB0 =0;
	BCF        PORTB+0, 0
;Slave_DA3.c,77 :: 		}
L_main13:
;Slave_DA3.c,79 :: 		if(Frame_System_Recive[6] == '1'){
	MOVF       _Frame_System_Recive+6, 0
	XORLW      49
	BTFSS      STATUS+0, 2
	GOTO       L_main14
;Slave_DA3.c,80 :: 		PORTB.RB4 =1;
	BSF        PORTB+0, 4
;Slave_DA3.c,81 :: 		}
L_main14:
;Slave_DA3.c,82 :: 		if(Frame_System_Recive[6] == '0'){
	MOVF       _Frame_System_Recive+6, 0
	XORLW      48
	BTFSS      STATUS+0, 2
	GOTO       L_main15
;Slave_DA3.c,83 :: 		PORTB.RB4 =0;
	BCF        PORTB+0, 4
;Slave_DA3.c,84 :: 		}
L_main15:
;Slave_DA3.c,86 :: 		if(Frame_System_Recive[7] == '1'){
	MOVF       _Frame_System_Recive+7, 0
	XORLW      49
	BTFSS      STATUS+0, 2
	GOTO       L_main16
;Slave_DA3.c,87 :: 		PORTB.RB5 =1;
	BSF        PORTB+0, 5
;Slave_DA3.c,88 :: 		}
L_main16:
;Slave_DA3.c,89 :: 		if(Frame_System_Recive[7] == '0'){
	MOVF       _Frame_System_Recive+7, 0
	XORLW      48
	BTFSS      STATUS+0, 2
	GOTO       L_main17
;Slave_DA3.c,90 :: 		PORTB.RB5 =0;
	BCF        PORTB+0, 5
;Slave_DA3.c,91 :: 		}
L_main17:
;Slave_DA3.c,93 :: 		}
L_main11:
;Slave_DA3.c,94 :: 		}
L_main8:
;Slave_DA3.c,99 :: 		}
	GOTO       L_main6
;Slave_DA3.c,100 :: 		}
L_end_main:
	GOTO       $+0
; end of _main

_Sys_getFrame:

;Slave_DA3.c,103 :: 		unsigned char Sys_getFrame(unsigned char *pData, unsigned char CharterStart, unsigned char CharterStop, unsigned char len){
;Slave_DA3.c,109 :: 		Frame_status = 0;
	CLRF       Sys_getFrame_Frame_status_L0+0
;Slave_DA3.c,111 :: 		while((RxUart1.Len > 0)&&(Frame_status == 0)){
L_Sys_getFrame18:
	MOVF       _RxUart1+1, 0
	SUBLW      0
	BTFSC      STATUS+0, 0
	GOTO       L_Sys_getFrame19
	MOVF       Sys_getFrame_Frame_status_L0+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_Sys_getFrame19
L__Sys_getFrame39:
;Slave_DA3.c,112 :: 		Read_status = Rd_buff(&RxUart1,30, &Data_val);
	MOVLW      _RxUart1+0
	MOVWF      FARG_Rd_buff_buf_prt+0
	MOVLW      30
	MOVWF      FARG_Rd_buff_buf_size+0
	MOVLW      Sys_getFrame_Data_val_L0+0
	MOVWF      FARG_Rd_buff_oval_prt+0
	CALL       _Rd_buff+0
;Slave_DA3.c,114 :: 		if(Read_status == 1){
	MOVF       R0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_Sys_getFrame22
;Slave_DA3.c,116 :: 		for( i = 0;i < len;i++){
	CLRF       Sys_getFrame_i_L0+0
L_Sys_getFrame23:
	MOVF       FARG_Sys_getFrame_len+0, 0
	SUBWF      Sys_getFrame_i_L0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_Sys_getFrame24
;Slave_DA3.c,117 :: 		Shiff_frame_sys[len - i -1] = Shiff_frame_sys[len - i - 2];
	MOVF       Sys_getFrame_i_L0+0, 0
	SUBWF      FARG_Sys_getFrame_len+0, 0
	MOVWF      R3+0
	CLRF       R3+1
	BTFSS      STATUS+0, 0
	DECF       R3+1, 1
	MOVLW      1
	SUBWF      R3+0, 0
	MOVWF      R0+0
	MOVLW      0
	BTFSS      STATUS+0, 0
	ADDLW      1
	SUBWF      R3+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	ADDLW      _Shiff_frame_sys+0
	MOVWF      R2+0
	MOVLW      2
	SUBWF      R3+0, 0
	MOVWF      R0+0
	MOVLW      0
	BTFSS      STATUS+0, 0
	ADDLW      1
	SUBWF      R3+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	ADDLW      _Shiff_frame_sys+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R2+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;Slave_DA3.c,116 :: 		for( i = 0;i < len;i++){
	INCF       Sys_getFrame_i_L0+0, 1
;Slave_DA3.c,118 :: 		}
	GOTO       L_Sys_getFrame23
L_Sys_getFrame24:
;Slave_DA3.c,120 :: 		Shiff_frame_sys[0] = Data_val;
	MOVF       Sys_getFrame_Data_val_L0+0, 0
	MOVWF      _Shiff_frame_sys+0
;Slave_DA3.c,122 :: 		if((Shiff_frame_sys[len-1] == CharterStart) && (Shiff_frame_sys[0] == CharterStop)){
	MOVLW      1
	SUBWF      FARG_Sys_getFrame_len+0, 0
	MOVWF      R0+0
	CLRF       R0+1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVF       R0+0, 0
	ADDLW      _Shiff_frame_sys+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	XORWF      FARG_Sys_getFrame_CharterStart+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_Sys_getFrame28
	MOVF       _Shiff_frame_sys+0, 0
	XORWF      FARG_Sys_getFrame_CharterStop+0, 0
	BTFSS      STATUS+0, 2
	GOTO       L_Sys_getFrame28
L__Sys_getFrame38:
;Slave_DA3.c,124 :: 		for( k = 0; k < len;k++){
	CLRF       Sys_getFrame_k_L0+0
L_Sys_getFrame29:
	MOVF       FARG_Sys_getFrame_len+0, 0
	SUBWF      Sys_getFrame_k_L0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_Sys_getFrame30
;Slave_DA3.c,125 :: 		*pData = Shiff_frame_sys[len - k - 1];
	MOVF       Sys_getFrame_k_L0+0, 0
	SUBWF      FARG_Sys_getFrame_len+0, 0
	MOVWF      R0+0
	CLRF       R0+1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVLW      1
	SUBWF      R0+0, 1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVF       R0+0, 0
	ADDLW      _Shiff_frame_sys+0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       FARG_Sys_getFrame_pData+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;Slave_DA3.c,126 :: 		pData++;
	INCF       FARG_Sys_getFrame_pData+0, 1
;Slave_DA3.c,127 :: 		Shiff_frame_sys[len - k - 1] = 0;
	MOVF       Sys_getFrame_k_L0+0, 0
	SUBWF      FARG_Sys_getFrame_len+0, 0
	MOVWF      R0+0
	CLRF       R0+1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVLW      1
	SUBWF      R0+0, 1
	BTFSS      STATUS+0, 0
	DECF       R0+1, 1
	MOVF       R0+0, 0
	ADDLW      _Shiff_frame_sys+0
	MOVWF      FSR
	CLRF       INDF+0
;Slave_DA3.c,124 :: 		for( k = 0; k < len;k++){
	INCF       Sys_getFrame_k_L0+0, 1
;Slave_DA3.c,128 :: 		}
	GOTO       L_Sys_getFrame29
L_Sys_getFrame30:
;Slave_DA3.c,129 :: 		Frame_status = 1;
	MOVLW      1
	MOVWF      Sys_getFrame_Frame_status_L0+0
;Slave_DA3.c,130 :: 		}
	GOTO       L_Sys_getFrame32
L_Sys_getFrame28:
;Slave_DA3.c,131 :: 		else {Frame_status = 0;}
	CLRF       Sys_getFrame_Frame_status_L0+0
L_Sys_getFrame32:
;Slave_DA3.c,132 :: 		}
L_Sys_getFrame22:
;Slave_DA3.c,133 :: 		}
	GOTO       L_Sys_getFrame18
L_Sys_getFrame19:
;Slave_DA3.c,135 :: 		return Frame_status;
	MOVF       Sys_getFrame_Frame_status_L0+0, 0
	MOVWF      R0+0
;Slave_DA3.c,136 :: 		}
L_end_Sys_getFrame:
	RETURN
; end of _Sys_getFrame

_Rd_buff:

;Slave_DA3.c,139 :: 		unsigned char Rd_buff(RxBuffUart_T *buf_prt,unsigned char buf_size,  unsigned char *oval_prt){
;Slave_DA3.c,140 :: 		unsigned char rd_status = 0xff;
	MOVLW      255
	MOVWF      Rd_buff_rd_status_L0+0
;Slave_DA3.c,142 :: 		if(buf_prt->Len > 0){
	INCF       FARG_Rd_buff_buf_prt+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	SUBLW      0
	BTFSC      STATUS+0, 0
	GOTO       L_Rd_buff33
;Slave_DA3.c,143 :: 		*oval_prt = buf_prt->RxBuff[buf_prt->Idx];
	MOVLW      2
	ADDWF      FARG_Rd_buff_buf_prt+0, 0
	MOVWF      R1+0
	MOVF       FARG_Rd_buff_buf_prt+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R0+0, 0
	ADDWF      R1+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVF       FARG_Rd_buff_oval_prt+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;Slave_DA3.c,144 :: 		buf_prt->Idx = (buf_prt->Idx + 1)% (buf_size );
	MOVF       FARG_Rd_buff_buf_prt+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	MOVLW      1
	ADDWF      R0+0, 1
	CLRF       R0+1
	BTFSC      STATUS+0, 0
	INCF       R0+1, 1
	MOVF       FARG_Rd_buff_buf_size+0, 0
	MOVWF      R4+0
	CLRF       R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       FARG_Rd_buff_buf_prt+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;Slave_DA3.c,145 :: 		buf_prt->Len--;
	INCF       FARG_Rd_buff_buf_prt+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      FSR
	DECF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;Slave_DA3.c,146 :: 		rd_status = 1;
	MOVLW      1
	MOVWF      Rd_buff_rd_status_L0+0
;Slave_DA3.c,147 :: 		}
	GOTO       L_Rd_buff34
L_Rd_buff33:
;Slave_DA3.c,149 :: 		*oval_prt = 0;
	MOVF       FARG_Rd_buff_oval_prt+0, 0
	MOVWF      FSR
	CLRF       INDF+0
;Slave_DA3.c,150 :: 		rd_status = 0;
	CLRF       Rd_buff_rd_status_L0+0
;Slave_DA3.c,151 :: 		}
L_Rd_buff34:
;Slave_DA3.c,153 :: 		return rd_status;
	MOVF       Rd_buff_rd_status_L0+0, 0
	MOVWF      R0+0
;Slave_DA3.c,154 :: 		}
L_end_Rd_buff:
	RETURN
; end of _Rd_buff

_Wr_buff:

;Slave_DA3.c,156 :: 		unsigned char Wr_buff(RxBuffUart_T *buf_prt,unsigned char buf_size, unsigned char val)
;Slave_DA3.c,160 :: 		new_idx = (buf_prt->Idx + buf_prt->Len) % (buf_size ); // rollover buff
	MOVF       FARG_Wr_buff_buf_prt+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      R0+0
	INCF       FARG_Wr_buff_buf_prt+0, 0
	MOVWF      FLOC__Wr_buff+0
	MOVF       FLOC__Wr_buff+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	ADDWF      R0+0, 1
	CLRF       R0+1
	BTFSC      STATUS+0, 0
	INCF       R0+1, 1
	MOVF       FARG_Wr_buff_buf_size+0, 0
	MOVWF      R4+0
	CLRF       R4+1
	CALL       _Div_16x16_S+0
	MOVF       R8+0, 0
	MOVWF      R0+0
	MOVF       R8+1, 0
	MOVWF      R0+1
	MOVF       R0+0, 0
	MOVWF      Wr_buff_new_idx_L0+0
;Slave_DA3.c,161 :: 		if(buf_prt->Len < buf_size){
	MOVF       FLOC__Wr_buff+0, 0
	MOVWF      FSR
	MOVF       FARG_Wr_buff_buf_size+0, 0
	SUBWF      INDF+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_Wr_buff35
;Slave_DA3.c,162 :: 		buf_prt->RxBuff[new_idx] = val;
	MOVLW      2
	ADDWF      FARG_Wr_buff_buf_prt+0, 0
	MOVWF      R0+0
	MOVF       Wr_buff_new_idx_L0+0, 0
	ADDWF      R0+0, 0
	MOVWF      FSR
	MOVF       FARG_Wr_buff_val+0, 0
	MOVWF      INDF+0
;Slave_DA3.c,163 :: 		buf_prt->Len++;
	INCF       FARG_Wr_buff_buf_prt+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      FSR
	INCF       INDF+0, 0
	MOVWF      R0+0
	MOVF       R1+0, 0
	MOVWF      FSR
	MOVF       R0+0, 0
	MOVWF      INDF+0
;Slave_DA3.c,164 :: 		wr_status = 1;
	MOVLW      1
	MOVWF      Wr_buff_wr_status_L0+0
;Slave_DA3.c,165 :: 		}
	GOTO       L_Wr_buff36
L_Wr_buff35:
;Slave_DA3.c,166 :: 		else{wr_status = 0;}
	CLRF       Wr_buff_wr_status_L0+0
L_Wr_buff36:
;Slave_DA3.c,168 :: 		return wr_status;
	MOVF       Wr_buff_wr_status_L0+0, 0
	MOVWF      R0+0
;Slave_DA3.c,169 :: 		}
L_end_Wr_buff:
	RETURN
; end of _Wr_buff
