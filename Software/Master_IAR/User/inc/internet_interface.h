/**
  ******************************************************************************
  * @file     internet_interface.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __INTERNET_INTERFACE
#define __INTERNET_INTERFACE
/* Includes ------------------------------------------------------------------*/
#include "uart2_internet.h"
/* Private typedef -----------------------------------------------------------*/
typedef enum{
  INTERNET_RX_IDLE            = (uint8_t) 0,
  INTERNET_RX_PARSER          = (uint8_t) 1
}internet_state_t;


/* Private define ------------------------------------------------------------*/

void internet_init(void);
internet_state_t App_Internet_State(internet_state_t curr_state,
                                internet_state_t *old_state);
void  App_Internet_Poll(void);
uint8_t internet_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len);


#endif