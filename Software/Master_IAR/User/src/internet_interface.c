/**
  ******************************************************************************
  * @file     internet_interface.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

/* Includes ------------------------------------------------------------------*/
#include "internet_interface.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern  uint8_t flag_internet_send_to_system ;
extern  uint8_t flag_sys_send_to_internet;

static uint8_t Frame_Internet_Recive[9];
extern  uint8_t Frame_Internet_Send[9];
extern  uint8_t Frame_Systerm_Send[9];

static uint8_t Shiff_frame[9];
static uint8_t flag_update_internet = 0;
static internet_state_t state_internet = INTERNET_RX_IDLE;
static internet_state_t old_state_internet = INTERNET_RX_IDLE;
/*******************************************************************************
* Function name: internet_init
* Description:
* Input: 
* Return:
*******************************************************************************/
void internet_init(void){
  UART2_Internet_Init();

}

internet_state_t App_Internet_State(internet_state_t curr_state,
                                internet_state_t *old_state){
  internet_state_t state;
  *old_state = curr_state;
  switch(curr_state){
  case INTERNET_RX_IDLE:
    if(flag_update_internet == 1){
      state = INTERNET_RX_PARSER;
    }
    else state = INTERNET_RX_IDLE;
    break;
  case INTERNET_RX_PARSER:
    state = INTERNET_RX_IDLE;
    break;
  default:
    state = INTERNET_RX_IDLE;
    break;
  
  }
  return state;
}

void  App_Internet_Poll(void){
  flag_update_internet = internet_getFrame(&Frame_Internet_Recive[0],'S','E',9);
  state_internet = App_Internet_State(state_internet,&old_state_internet);
  if(state_internet == INTERNET_RX_IDLE){
    flag_update_internet = 0;
  }
  if((state_internet != old_state_internet)&&(state_internet == INTERNET_RX_PARSER)){
    if((Frame_Internet_Recive[1] == '1') && (Frame_Internet_Recive[2] == 'I') && (Frame_Internet_Recive[3] == '1')){
      // set frame de gui trang thai thiet bi cho system
      
      Frame_Systerm_Send[0] = 'S';
      Frame_Systerm_Send[1] = '0';
      Frame_Systerm_Send[2] = 'D';
      Frame_Systerm_Send[3] = '0';
      Frame_Systerm_Send[4] = '0';
      if(Frame_Systerm_Send[5] != Frame_Internet_Recive[5]){
        Frame_Systerm_Send[5] = Frame_Internet_Recive[5];
        flag_internet_send_to_system = 1;
      }
      if(Frame_Systerm_Send[6] != Frame_Internet_Recive[6]){
        Frame_Systerm_Send[6] = Frame_Internet_Recive[6];
        flag_internet_send_to_system = 1;
      }
      if(Frame_Systerm_Send[7] != Frame_Internet_Recive[7]){
        Frame_Systerm_Send[7] = Frame_Internet_Recive[7];
        flag_internet_send_to_system = 1;
      }
      
      Frame_Systerm_Send[8] = 'E';
    }
  }
  if(flag_sys_send_to_internet == 1){
    flag_sys_send_to_internet = 0;
    UART2_Internet_write(&Frame_Internet_Send[0], 9);
  }
}

uint8_t internet_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len){
  uint8_t Read_status;
  uint8_t Frame_status;
  uint8_t Data_val ;
  uint8_t i,k;

  Frame_status = 0;
  // lay data tu Buffer va kiem tra xem co Frame gui ve khong.
  while((RxUart2.Len > 0)&&(Frame_status == 0)){
      Read_status = Rd_buff(&RxUart2,RxBuffUART_size, &Data_val);
      // kiem tra data new
      if(Read_status == 1){
        // Shiff toan bo data sang phai 1 byte
        for( i = 0;i < len;i++){
           Shiff_frame[len - i -1] = Shiff_frame[len - i - 2];
        }
        // day data moi vao
        Shiff_frame[0] = Data_val;
        // kiem tra Frame dung la 1 frame
            if((Shiff_frame[len-1] == CharterStart) && (Shiff_frame[0] == CharterStop)){
              // truyen frame len tren
              for( k = 0; k < len;k++){
                *pData = Shiff_frame[len - k - 1];
                pData++;
                Shiff_frame[len - k - 1] = 0;
              }
              Frame_status = 1;
            }
            else {Frame_status = 0;}
      }
  }
  // return 1 neu co Frame, 0 neu khong co Frame.
  return Frame_status;
}