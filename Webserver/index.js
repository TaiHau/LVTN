var express = require('express');
var app = express();
var fetch = require('node-fetch');
var expressHbs  = require('express-handlebars');
assert = require('assert');

const path = require('path');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.set('view engine', 'handlebars');
app.engine('handlebars', expressHbs({defaultLayout: 'main'}));
app.use('/', express.static('D:/NAM 4/LVTN/LVTN/Webserver/public'));

//khai bao cho mongodb
var MongoClient = require('mongodb').MongoClient;

var StatusChangedFlag = {};
    StatusChangedFlag.changedFlag = "false";

var tendangnhap = "Hau";
var matkhau = "1313";
var loginFlag = false;

var Power = 30;
var d1 = new Date();
dateNow =  d1.getDate();
monthNow = d1.getMonth() + 1;
yearNow =  d1.getFullYear();
var dateFilter = dateNow;
var monthFilter = monthNow;
var yearFilter = yearNow;

var chartTime = [];
var chartPower =['30','40'];
var chartCount = 0;

var StateTB = {};
    StateTB.TB1 = "off";
    StateTB.TB2 = "off";
    StateTB.TB3 = "off";
    StateTB.TB4 = "off";
    //StateTB.nhietdo = "30";
    //StateTB.doam = "60";

    StateTB.TB1timeon = "00:00";
    StateTB.TB2timeon = "00:00";
    StateTB.TB3timeon = "00:00";
    StateTB.TB4timeon = "00:00";

    StateTB.TB1timeoff = "00:00";
    StateTB.TB2timeoff = "00:00";
    StateTB.TB3timeoff = "00:00";
    StateTB.TB4timeoff = "00:00";

//------------------------------------------------------------------------
// Dang nhap vao he thong
app.get('/',function(req,res){
    res.render('login');
});
app.get('/login',function(req,res){
    res.render('login');
});
app.post('/dangnhap',function(req,res){
    if(req.body.username === tendangnhap && req.body.password === matkhau){
        loginFlag = true;
        res.redirect('home');
    }
    else{
        res.redirect('login');
    }
});
//-------------------------------------------------------------------------

MongoClient.connect('mongodb://localhost:27017/', function(err, db){
    assert.equal(null,err);
    console.log("Successfully connect MongoDB");
    var projection = {"date" :1, "year" :1,"month" :1, "time" :1, "P":1, "_id":0}; //"deviceID": 1, 
    //db.collection('test2').insertOne({"deviceID": "D04", "date": d.getDate(), "month": d.getMonth(), "year": d.getFullYear(), "time": d.getHours() + "." + d.getMinutes(), "P": req.query.Power})
    var d = new Date();
    var dbo = db.db("LVTNtest");
    var cursor = dbo.collection('test2').find({time: {$gt: '1.20'},date: {$eq: d.getDate()},month: {$eq: d.getMonth()+1},year: {$eq: d.getFullYear()}})
    cursor.project(projection)
    cursor.forEach(
        function(doc) {
            chartTime[chartCount] = doc.time;
            chartPower[chartCount] = doc.P;
            chartCount++;
            console.log(doc.year);
        },
        function(err) {
            assert.equal(err, null);
            return db.close();
        }
    );   
});

app.get('/home',function(req,res){
    if(loginFlag === true){
        MongoClient.connect('mongodb://localhost:27017/', function(err, db){
            chartCount = 0;
            chartTime = [];
            chartPower = [];
            assert.equal(null,err);
            var projection = {"date" :1, "year" :1,"month" :1, "time" :1, "P":1, "_id":0};
            var d = new Date();
            var dbo = db.db("LVTNtest");
            var cursor = dbo.collection('test2').find({time: {$gt: '1.20'},date: {$eq: d.getDate()},month: {$eq: d.getMonth()+1},year: {$eq: d.getFullYear()}})
            cursor.project(projection)
            cursor.forEach(
                function(doc) {
                    chartTime[chartCount] = doc.time;
                    chartPower[chartCount] = doc.P;
                    chartCount++;
                    console.log(doc.year);
                },
                function(err) {
                    assert.equal(err, null);
                    return db.close();
                }
            ); 
        })
        
        res.render('home',{
            chartTime: chartTime,
            chartPower: chartPower,
        });
        
    }
    else res.render('login');
});

app.get('/control',function(req,res){
    if(loginFlag === true){
        res.render('control',{
            TB1state: (StateTB.TB1 === "on") ? 'ON' : 'OFF',
            TB2state: (StateTB.TB2 === "on") ? 'ON' : 'OFF',
            TB3state: (StateTB.TB3 === "on") ? 'ON' : 'OFF',
            TB4state: (StateTB.TB4 === "on") ? 'ON' : 'OFF',
        });
    }
    else res.render('login');
});
app.get('/sensor',function(req,res){
    if(loginFlag === true){
        res.render('sensor');
    }
    else res.render('login');
});
app.get('/SensorFromMaster',function(req,res){
    StateTB.nhietdo = req.query.nhietdo;
    StateTB.doam    = req.query.doam;
});
//------------------------------------------------------------------------
//Post trang thai thiet bi
app.get('/TB1', function(req,res) {
    StateTB.TB1 = (StateTB.TB1 === "on") ? "off" : "on";
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
});
app.post('/TB2', function(req,res) {
    StateTB.TB2 = (StateTB.TB2 === "on") ? "off" : "on";
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
});
app.post('/TB3', function(req,res) {
    StateTB.TB3 = (StateTB.TB3 === "on") ? "off" : "on";
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
});
app.post('/TB4', function(req,res) {
    StateTB.TB4 = (StateTB.TB4 === "on") ? "off" : "on";
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
});
// Hen gio cho cac thiet bi
app.get('/submitTheTimeDevice1', function(req,res){
    StateTB.TB1timeon = req.query.setTimeOn;
    StateTB.TB1timeoff = req.query.setTimeOff;
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
 });
 app.get('/submitTheTimeDevice2', function(req,res){
    StateTB.TB2timeon = req.query.setTimeOn;
    StateTB.TB2timeoff = req.query.setTimeOff;
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
 });
 app.get('/submitTheTimeDevice3', function(req,res){
    StateTB.TB3timeon = req.query.setTimeOn;
    StateTB.TB3timeoff = req.query.setTimeOff;
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
 });
 app.get('/submitTheTimeDevice4', function(req,res){
    StateTB.TB4timeon = req.query.setTimeOn;
    StateTB.TB4timeoff = req.query.setTimeOff;
    StatusChangedFlag.changedFlag = "true";
    res.redirect('/control');
 });
//Doc trang thai tu Master
app.get('/trangthaiTB',function(req,res){
    if(req.query.TB1)
        StateTB.TB1 = req.query.TB1;
    if(req.query.TB2)
        StateTB.TB2 = req.query.TB2;
    if(req.query.TB3)
        StateTB.TB3 = req.query.TB3;
    if(req.query.TB4)
        StateTB.TB4 = req.query.TB4;
});
app.get('/checkChangedFlag',function(req,res){
    if(req.query.device === "NodeMCU") {
        StatusChangedFlag.changedFlag = "false";
    }
    res.end(JSON.stringify(StatusChangedFlag));
});
// Chuoi Json cua thiet bi
app.get('/State',function(req,res){
    res.end(JSON.stringify(StateTB));
});
app.get('/readPowerFromSystem', function (req, res) {
    var d = new Date();
    Power = req.query.Power;
    MongoClient.connect('mongodb://localhost:27017/LVTNtest', function(err, db){
        assert.equal(null,err);
        db.collection('test2').insertOne({"deviceID": "D04", "date": d.getDate(), "month": d.getMonth()+1, "year": d.getFullYear(), "time": d.getHours() + "." + d.getMinutes(), "P": req.query.Power})
    });
});

app.get('/Power', function (req, res) {
    res.end(JSON.stringify(Power));
});
app.get('/chart', function (req, res) {
    if(loginFlag === true){
        MongoClient.connect('mongodb://localhost:27017/', function(err, db){
            assert.equal(null,err);
            chartCount = 0;
            chartTime = [];
            chartPower = [];
            var projection = {"date" :1, "year" :1,"month" :1, "time" :1, "P":1, "_id":0};
            var d = new Date();
            var dbo = db.db("LVTNtest");
            var cursor = dbo.collection('test2').find({date: {$eq: dateFilter},month: {$eq: monthFilter},year: {$eq: yearFilter}  })
            cursor.project(projection)
            cursor.forEach(
                function(doc) {
                    chartTime[chartCount] = doc.time;
                    chartPower[chartCount] = doc.P;
                    chartCount++;
                    console.log(doc.year);
                },
                function(err) {
                    assert.equal(err, null);
                    return db.close();
                }
            ); 
        })
        res.render('chart',{
            date: dateFilter,
            month: monthFilter,
            year: yearFilter,
            chartTime: chartTime,
            chartPower: chartPower
        });
    }
    else
        res.redirect('/');
});
app.get('/filterPower', function (req, res) {
    var a = req.query.chartChooseMonth + " " + req.query.chartChooseDate + " " + req.query.chartChooseYear;
    var b = new Date(a);
    dateFilter = b.getDate();
    monthFilter = b.getMonth()+1;
    yearFilter = b.getFullYear();
    res.redirect('/chart');
});


app.listen(3000);