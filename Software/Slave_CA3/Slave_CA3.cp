#line 1 "D:/NAM 4/LVTN/LVTN/Software/Slave_CA3/Slave_CA3.c"


unsigned char stt1 =0, stt2 =0,stt3 = 0,busy = 0;
char flagReceivedAllData = 0;
unsigned char count = 0, tempReceiveData,receiveData[9];
unsigned char sendData[9] ;
void RS485_send (unsigned char *dat_prt, unsigned char NumData);



void interrupt()
{
 if(PIR1.RCIF)
 {
 while(uart1_data_ready()==0);
 if(uart1_data_ready()==1)
 {
 tempReceiveData = UART1_Read();
 if(tempReceiveData == 'S')
 {
 busy = 1;
 count = 0;
 receiveData[count] = tempReceiveData;
 count++;
 }
 if(tempReceiveData !='S' && tempReceiveData !='E')
 {
 receiveData[count] = tempReceiveData;
 count++;
 }
 if(tempReceiveData == 'E')
 {
 receiveData[count] = tempReceiveData;
 count=0;
 flagReceivedAllData = 1;
 busy = 0;
 }
 }
 }
}
void main() {
 TRISB.B0 =1;
 TRISB.B4 =1;
 TRISB.B5 =1;

 TRISB.B3 =0;

 UART1_Init(9600);
 Delay_ms(100);

 RCIE_bit = 1;
 TXIE_bit = 0;
 PEIE_bit = 1;
 GIE_bit = 1;

 sendData[0] = 'S';
 sendData[1] = '1';
 sendData[2] = 'B';
 sendData[3] = '0';
 sendData[4] = '0';
 sendData[5] = '0';
 sendData[6] = '0';
 sendData[7] = '0';
 sendData[8] = 'E';

 while(1)
 {

 if(Button(&PORTB, 0, 20, 0))
 {

 while(PORTB.B0 == 0);
 stt1 = ~stt1;

 if(stt1 != 0)
 {
 sendData[5] = '1';
 while(busy == 1){
 ;
 }
 RS485_send(&sendData[0],9);
 }
 else if(stt1 == 0)
 {
 sendData[5] = '0';
 while(busy == 1){
 ;
 }
 RS485_send(&sendData[0],9);
 }
 }


 if(Button(&PORTB, 4, 20, 0))
 {

 while(PORTB.B4 == 0);
 stt2 = ~stt2;

 if(stt2 != 0)
 {
 sendData[6] = '1';
 while(busy == 1){
 ;
 }
 RS485_send(&sendData[0],9);
 }
 else if(stt2 == 0)
 {
 sendData[6] = '0';
 while(busy == 1){
 ;
 }
 RS485_send(&sendData[0],9);
 }
 }

 if(Button(&PORTB, 5, 20, 0))
 {

 while(PORTB.B5 == 0);
 stt3 = ~stt3;

 if(stt3 != 0)
 {
 sendData[7] = '1';
 while(busy == 1){
 ;
 }
 RS485_send(&sendData[0],9);
 }
 else if(stt3 == 0)
 {
 sendData[7] = '0';
 while(busy == 1){
 ;
 }
 RS485_send(&sendData[0],9);
 }
 }

 }
}
#line 158 "D:/NAM 4/LVTN/LVTN/Software/Slave_CA3/Slave_CA3.c"
void RS485_send (unsigned char *dat_prt, unsigned char NumData)
{
 unsigned char i;
 PORTB.RB3 =1;
 Delay_ms(100);
 for (i=0; i<NumData;i++){
 while(UART1_Tx_Idle()==0);
 UART1_Write(*dat_prt);
 dat_prt++;
 }
 Delay_ms(100);
 PORTB.RB3 =0;
}
