/**
  ******************************************************************************
  * @file     Uart2_internet.c
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/ 

/* Includes ------------------------------------------------------------------*/
#include "uart2_internet.h"


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
 RxBuffUart_T  RxUart2;

/* Private function prototypes -----------------------------------------------*/

/*******************************************************************************
* Function name: BSP_Si4463_stm8s_Init
* Description:
* Input: 
* Return:
*******************************************************************************/
void UART2_Internet_Init(void){
  GPIO_InitTypeDef GPIO_InitStructure;

  
  /* Configure USART2 Rx as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
 
  
  /* Configure USART2 Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  
  USART_InitTypeDef USART_InitStructure;
  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  
  USART_DeInit(USART2);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
  USART_Init(USART2,&USART_InitStructure);
  
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
  USART_Cmd(USART2,ENABLE);
}
/*******************************************************************************
* Function name: BSP_Si4463_stm8s_Init
* Description:
* Input: 
* Return:
*******************************************************************************/
void UART2_Internet_Read_isr(void){
   //--------new
   Wr_buff(&RxUart2,RxBuffUART_size,(uint8_t)USART_ReceiveData(USART2));
}


void UART2_Internet_write(uint8_t *iData,uint8_t NumByte){
  for(uint8_t i = 0;i < NumByte;i++){
    USART_SendData(USART2,*iData);
    while(USART_GetFlagStatus(USART2,USART_FLAG_TXE)==RESET);
    iData++;
  }
}
//------------------------------------------------------------------------------