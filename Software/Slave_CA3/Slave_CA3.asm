
_interrupt:
	MOVWF      R15+0
	SWAPF      STATUS+0, 0
	CLRF       STATUS+0
	MOVWF      ___saveSTATUS+0
	MOVF       PCLATH+0, 0
	MOVWF      ___savePCLATH+0
	CLRF       PCLATH+0

;Slave_CA3.c,11 :: 		void interrupt()
;Slave_CA3.c,13 :: 		if(PIR1.RCIF)
	BTFSS      PIR1+0, 5
	GOTO       L_interrupt0
;Slave_CA3.c,15 :: 		while(uart1_data_ready()==0);
L_interrupt1:
	CALL       _UART1_Data_Ready+0
	MOVF       R0+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt2
	GOTO       L_interrupt1
L_interrupt2:
;Slave_CA3.c,16 :: 		if(uart1_data_ready()==1)
	CALL       _UART1_Data_Ready+0
	MOVF       R0+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt3
;Slave_CA3.c,18 :: 		tempReceiveData = UART1_Read();
	CALL       _UART1_Read+0
	MOVF       R0+0, 0
	MOVWF      _tempReceiveData+0
;Slave_CA3.c,19 :: 		if(tempReceiveData == 'S')
	MOVF       R0+0, 0
	XORLW      83
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt4
;Slave_CA3.c,21 :: 		busy = 1;
	MOVLW      1
	MOVWF      _busy+0
;Slave_CA3.c,22 :: 		count = 0;
	CLRF       _count+0
;Slave_CA3.c,23 :: 		receiveData[count] = tempReceiveData;
	MOVF       _count+0, 0
	ADDLW      _receiveData+0
	MOVWF      FSR
	MOVF       _tempReceiveData+0, 0
	MOVWF      INDF+0
;Slave_CA3.c,24 :: 		count++;
	INCF       _count+0, 1
;Slave_CA3.c,25 :: 		}
L_interrupt4:
;Slave_CA3.c,26 :: 		if(tempReceiveData !='S' && tempReceiveData !='E')
	MOVF       _tempReceiveData+0, 0
	XORLW      83
	BTFSC      STATUS+0, 2
	GOTO       L_interrupt7
	MOVF       _tempReceiveData+0, 0
	XORLW      69
	BTFSC      STATUS+0, 2
	GOTO       L_interrupt7
L__interrupt49:
;Slave_CA3.c,28 :: 		receiveData[count] = tempReceiveData;
	MOVF       _count+0, 0
	ADDLW      _receiveData+0
	MOVWF      FSR
	MOVF       _tempReceiveData+0, 0
	MOVWF      INDF+0
;Slave_CA3.c,29 :: 		count++;
	INCF       _count+0, 1
;Slave_CA3.c,30 :: 		}
L_interrupt7:
;Slave_CA3.c,31 :: 		if(tempReceiveData == 'E')
	MOVF       _tempReceiveData+0, 0
	XORLW      69
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt8
;Slave_CA3.c,33 :: 		receiveData[count] = tempReceiveData;
	MOVF       _count+0, 0
	ADDLW      _receiveData+0
	MOVWF      FSR
	MOVF       _tempReceiveData+0, 0
	MOVWF      INDF+0
;Slave_CA3.c,34 :: 		count=0;
	CLRF       _count+0
;Slave_CA3.c,35 :: 		flagReceivedAllData = 1;
	MOVLW      1
	MOVWF      _flagReceivedAllData+0
;Slave_CA3.c,36 :: 		busy = 0;
	CLRF       _busy+0
;Slave_CA3.c,37 :: 		}
L_interrupt8:
;Slave_CA3.c,38 :: 		}
L_interrupt3:
;Slave_CA3.c,39 :: 		}
L_interrupt0:
;Slave_CA3.c,40 :: 		}
L_end_interrupt:
L__interrupt51:
	MOVF       ___savePCLATH+0, 0
	MOVWF      PCLATH+0
	SWAPF      ___saveSTATUS+0, 0
	MOVWF      STATUS+0
	SWAPF      R15+0, 1
	SWAPF      R15+0, 0
	RETFIE
; end of _interrupt

_main:

;Slave_CA3.c,41 :: 		void main() {
;Slave_CA3.c,42 :: 		TRISB.B0 =1;
	BSF        TRISB+0, 0
;Slave_CA3.c,43 :: 		TRISB.B4 =1;
	BSF        TRISB+0, 4
;Slave_CA3.c,44 :: 		TRISB.B5 =1;
	BSF        TRISB+0, 5
;Slave_CA3.c,46 :: 		TRISB.B3 =0;                         //Bit RS485
	BCF        TRISB+0, 3
;Slave_CA3.c,48 :: 		UART1_Init(9600);
	MOVLW      129
	MOVWF      SPBRG+0
	BSF        TXSTA+0, 2
	CALL       _UART1_Init+0
;Slave_CA3.c,49 :: 		Delay_ms(100);
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_main9:
	DECFSZ     R13+0, 1
	GOTO       L_main9
	DECFSZ     R12+0, 1
	GOTO       L_main9
	DECFSZ     R11+0, 1
	GOTO       L_main9
	NOP
	NOP
;Slave_CA3.c,51 :: 		RCIE_bit = 1;                        // enable interrupt on UART1 receive
	BSF        RCIE_bit+0, BitPos(RCIE_bit+0)
;Slave_CA3.c,52 :: 		TXIE_bit = 0;                        // disable interrupt on UART1 transmit
	BCF        TXIE_bit+0, BitPos(TXIE_bit+0)
;Slave_CA3.c,53 :: 		PEIE_bit = 1;                        // enable peripheral interrupts
	BSF        PEIE_bit+0, BitPos(PEIE_bit+0)
;Slave_CA3.c,54 :: 		GIE_bit = 1;                         // enable all interrupts
	BSF        GIE_bit+0, BitPos(GIE_bit+0)
;Slave_CA3.c,56 :: 		sendData[0] = 'S';
	MOVLW      83
	MOVWF      _sendData+0
;Slave_CA3.c,57 :: 		sendData[1] = '1';
	MOVLW      49
	MOVWF      _sendData+1
;Slave_CA3.c,58 :: 		sendData[2] = 'B';
	MOVLW      66
	MOVWF      _sendData+2
;Slave_CA3.c,59 :: 		sendData[3] = '0';
	MOVLW      48
	MOVWF      _sendData+3
;Slave_CA3.c,60 :: 		sendData[4] = '0';
	MOVLW      48
	MOVWF      _sendData+4
;Slave_CA3.c,61 :: 		sendData[5] = '0';
	MOVLW      48
	MOVWF      _sendData+5
;Slave_CA3.c,62 :: 		sendData[6] = '0';
	MOVLW      48
	MOVWF      _sendData+6
;Slave_CA3.c,63 :: 		sendData[7] = '0';
	MOVLW      48
	MOVWF      _sendData+7
;Slave_CA3.c,64 :: 		sendData[8] = 'E';
	MOVLW      69
	MOVWF      _sendData+8
;Slave_CA3.c,66 :: 		while(1)
L_main10:
;Slave_CA3.c,69 :: 		if(Button(&PORTB, 0, 20, 0))
	MOVLW      PORTB+0
	MOVWF      FARG_Button_port+0
	CLRF       FARG_Button_pin+0
	MOVLW      20
	MOVWF      FARG_Button_time_ms+0
	CLRF       FARG_Button_active_state+0
	CALL       _Button+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main12
;Slave_CA3.c,72 :: 		while(PORTB.B0 == 0);            // doi nha phim
L_main13:
	BTFSC      PORTB+0, 0
	GOTO       L_main14
	GOTO       L_main13
L_main14:
;Slave_CA3.c,73 :: 		stt1 = ~stt1;                        // kt = 1, on led, kt = 0, off led
	COMF       _stt1+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      _stt1+0
;Slave_CA3.c,75 :: 		if(stt1 != 0)
	MOVF       R1+0, 0
	XORLW      0
	BTFSC      STATUS+0, 2
	GOTO       L_main15
;Slave_CA3.c,77 :: 		sendData[5] = '1';
	MOVLW      49
	MOVWF      _sendData+5
;Slave_CA3.c,78 :: 		while(busy == 1){
L_main16:
	MOVF       _busy+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main17
;Slave_CA3.c,80 :: 		}
	GOTO       L_main16
L_main17:
;Slave_CA3.c,81 :: 		RS485_send(&sendData[0],9);
	MOVLW      _sendData+0
	MOVWF      FARG_RS485_send_dat_prt+0
	MOVLW      9
	MOVWF      FARG_RS485_send_NumData+0
	CALL       _RS485_send+0
;Slave_CA3.c,82 :: 		}
	GOTO       L_main18
L_main15:
;Slave_CA3.c,83 :: 		else if(stt1 == 0)
	MOVF       _stt1+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main19
;Slave_CA3.c,85 :: 		sendData[5] = '0';
	MOVLW      48
	MOVWF      _sendData+5
;Slave_CA3.c,86 :: 		while(busy == 1){
L_main20:
	MOVF       _busy+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main21
;Slave_CA3.c,88 :: 		}
	GOTO       L_main20
L_main21:
;Slave_CA3.c,89 :: 		RS485_send(&sendData[0],9);
	MOVLW      _sendData+0
	MOVWF      FARG_RS485_send_dat_prt+0
	MOVLW      9
	MOVWF      FARG_RS485_send_NumData+0
	CALL       _RS485_send+0
;Slave_CA3.c,90 :: 		}
L_main19:
L_main18:
;Slave_CA3.c,91 :: 		}
L_main12:
;Slave_CA3.c,94 :: 		if(Button(&PORTB, 4, 20, 0))
	MOVLW      PORTB+0
	MOVWF      FARG_Button_port+0
	MOVLW      4
	MOVWF      FARG_Button_pin+0
	MOVLW      20
	MOVWF      FARG_Button_time_ms+0
	CLRF       FARG_Button_active_state+0
	CALL       _Button+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main22
;Slave_CA3.c,97 :: 		while(PORTB.B4 == 0);            // doi nha phim
L_main23:
	BTFSC      PORTB+0, 4
	GOTO       L_main24
	GOTO       L_main23
L_main24:
;Slave_CA3.c,98 :: 		stt2 = ~stt2;                        // kt = 1, on led, kt = 0, off led
	COMF       _stt2+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      _stt2+0
;Slave_CA3.c,100 :: 		if(stt2 != 0)
	MOVF       R1+0, 0
	XORLW      0
	BTFSC      STATUS+0, 2
	GOTO       L_main25
;Slave_CA3.c,102 :: 		sendData[6] = '1';
	MOVLW      49
	MOVWF      _sendData+6
;Slave_CA3.c,103 :: 		while(busy == 1){
L_main26:
	MOVF       _busy+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main27
;Slave_CA3.c,105 :: 		}
	GOTO       L_main26
L_main27:
;Slave_CA3.c,106 :: 		RS485_send(&sendData[0],9);
	MOVLW      _sendData+0
	MOVWF      FARG_RS485_send_dat_prt+0
	MOVLW      9
	MOVWF      FARG_RS485_send_NumData+0
	CALL       _RS485_send+0
;Slave_CA3.c,107 :: 		}
	GOTO       L_main28
L_main25:
;Slave_CA3.c,108 :: 		else if(stt2 == 0)
	MOVF       _stt2+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main29
;Slave_CA3.c,110 :: 		sendData[6] = '0';
	MOVLW      48
	MOVWF      _sendData+6
;Slave_CA3.c,111 :: 		while(busy == 1){
L_main30:
	MOVF       _busy+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main31
;Slave_CA3.c,113 :: 		}
	GOTO       L_main30
L_main31:
;Slave_CA3.c,114 :: 		RS485_send(&sendData[0],9);
	MOVLW      _sendData+0
	MOVWF      FARG_RS485_send_dat_prt+0
	MOVLW      9
	MOVWF      FARG_RS485_send_NumData+0
	CALL       _RS485_send+0
;Slave_CA3.c,115 :: 		}
L_main29:
L_main28:
;Slave_CA3.c,116 :: 		}
L_main22:
;Slave_CA3.c,118 :: 		if(Button(&PORTB, 5, 20, 0))
	MOVLW      PORTB+0
	MOVWF      FARG_Button_port+0
	MOVLW      5
	MOVWF      FARG_Button_pin+0
	MOVLW      20
	MOVWF      FARG_Button_time_ms+0
	CLRF       FARG_Button_active_state+0
	CALL       _Button+0
	MOVF       R0+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main32
;Slave_CA3.c,121 :: 		while(PORTB.B5 == 0);            // doi nha phim
L_main33:
	BTFSC      PORTB+0, 5
	GOTO       L_main34
	GOTO       L_main33
L_main34:
;Slave_CA3.c,122 :: 		stt3 = ~stt3;                        // kt = 1, on led, kt = 0, off led
	COMF       _stt3+0, 0
	MOVWF      R1+0
	MOVF       R1+0, 0
	MOVWF      _stt3+0
;Slave_CA3.c,124 :: 		if(stt3 != 0)
	MOVF       R1+0, 0
	XORLW      0
	BTFSC      STATUS+0, 2
	GOTO       L_main35
;Slave_CA3.c,126 :: 		sendData[7] = '1';
	MOVLW      49
	MOVWF      _sendData+7
;Slave_CA3.c,127 :: 		while(busy == 1){
L_main36:
	MOVF       _busy+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main37
;Slave_CA3.c,129 :: 		}
	GOTO       L_main36
L_main37:
;Slave_CA3.c,130 :: 		RS485_send(&sendData[0],9);
	MOVLW      _sendData+0
	MOVWF      FARG_RS485_send_dat_prt+0
	MOVLW      9
	MOVWF      FARG_RS485_send_NumData+0
	CALL       _RS485_send+0
;Slave_CA3.c,131 :: 		}
	GOTO       L_main38
L_main35:
;Slave_CA3.c,132 :: 		else if(stt3 == 0)
	MOVF       _stt3+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_main39
;Slave_CA3.c,134 :: 		sendData[7] = '0';
	MOVLW      48
	MOVWF      _sendData+7
;Slave_CA3.c,135 :: 		while(busy == 1){
L_main40:
	MOVF       _busy+0, 0
	XORLW      1
	BTFSS      STATUS+0, 2
	GOTO       L_main41
;Slave_CA3.c,137 :: 		}
	GOTO       L_main40
L_main41:
;Slave_CA3.c,138 :: 		RS485_send(&sendData[0],9);
	MOVLW      _sendData+0
	MOVWF      FARG_RS485_send_dat_prt+0
	MOVLW      9
	MOVWF      FARG_RS485_send_NumData+0
	CALL       _RS485_send+0
;Slave_CA3.c,139 :: 		}
L_main39:
L_main38:
;Slave_CA3.c,140 :: 		}
L_main32:
;Slave_CA3.c,142 :: 		}
	GOTO       L_main10
;Slave_CA3.c,143 :: 		}
L_end_main:
	GOTO       $+0
; end of _main

_RS485_send:

;Slave_CA3.c,158 :: 		void RS485_send (unsigned char *dat_prt, unsigned char NumData)
;Slave_CA3.c,161 :: 		PORTB.RB3 =1;
	BSF        PORTB+0, 3
;Slave_CA3.c,162 :: 		Delay_ms(100);
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_RS485_send42:
	DECFSZ     R13+0, 1
	GOTO       L_RS485_send42
	DECFSZ     R12+0, 1
	GOTO       L_RS485_send42
	DECFSZ     R11+0, 1
	GOTO       L_RS485_send42
	NOP
	NOP
;Slave_CA3.c,163 :: 		for (i=0; i<NumData;i++){
	CLRF       RS485_send_i_L0+0
L_RS485_send43:
	MOVF       FARG_RS485_send_NumData+0, 0
	SUBWF      RS485_send_i_L0+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_RS485_send44
;Slave_CA3.c,164 :: 		while(UART1_Tx_Idle()==0);
L_RS485_send46:
	CALL       _UART1_Tx_Idle+0
	MOVF       R0+0, 0
	XORLW      0
	BTFSS      STATUS+0, 2
	GOTO       L_RS485_send47
	GOTO       L_RS485_send46
L_RS485_send47:
;Slave_CA3.c,165 :: 		UART1_Write(*dat_prt);
	MOVF       FARG_RS485_send_dat_prt+0, 0
	MOVWF      FSR
	MOVF       INDF+0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;Slave_CA3.c,166 :: 		dat_prt++;
	INCF       FARG_RS485_send_dat_prt+0, 1
;Slave_CA3.c,163 :: 		for (i=0; i<NumData;i++){
	INCF       RS485_send_i_L0+0, 1
;Slave_CA3.c,167 :: 		}
	GOTO       L_RS485_send43
L_RS485_send44:
;Slave_CA3.c,168 :: 		Delay_ms(100);
	MOVLW      3
	MOVWF      R11+0
	MOVLW      138
	MOVWF      R12+0
	MOVLW      85
	MOVWF      R13+0
L_RS485_send48:
	DECFSZ     R13+0, 1
	GOTO       L_RS485_send48
	DECFSZ     R12+0, 1
	GOTO       L_RS485_send48
	DECFSZ     R11+0, 1
	GOTO       L_RS485_send48
	NOP
	NOP
;Slave_CA3.c,169 :: 		PORTB.RB3 =0;
	BCF        PORTB+0, 3
;Slave_CA3.c,170 :: 		}
L_end_RS485_send:
	RETURN
; end of _RS485_send
