



 unsigned char Shiff_frame_sys[9];
typedef struct{
    unsigned char Idx;
    unsigned char Len;
    unsigned char RxBuff[30];
}RxBuffUart_T;
RxBuffUart_T  RxUart1;
   unsigned char Frame_System_Recive[9];




unsigned char Rd_buff(RxBuffUart_T *buf_prt,unsigned char buf_size, unsigned char *oval_prt);
unsigned char Wr_buff(RxBuffUart_T *buf_prt,unsigned char buf_size, unsigned char val);
unsigned char Sys_getFrame(unsigned char *pData, unsigned char CharterStart, unsigned char CharterStop, unsigned char len);

// Interrupt routine
void interrupt()
{

  if(PIR1.RCIF)
  {
     while(uart1_data_ready()==0);
     if(uart1_data_ready()==1)
     {

        Wr_buff(&RxUart1,30,UART1_Read());
     }
  }
}
void main() 
{
  TRISB.B0 =0;
  TRISB.B4 =0;
  TRISB.B5 =0;

  TRISB.B3 =0;                         //Bit RS485
  PORTB.B3 = 0;


  UART1_Init(9600);
  Delay_ms(100);

  RCIE_bit = 1;                        // enable interrupt on UART1 receive
  TXIE_bit = 0;                        // disable interrupt on UART1 transmit
  PEIE_bit = 1;                        // enable peripheral interrupts
  GIE_bit = 1;                         // enable all interrupts



  PORTB.RB0 =1;
  PORTB.RB4 =1;
  PORTB.RB5 =1;
  Delay_ms(500);
  PORTB.RB0 =0;
  PORTB.RB4 =0;
  PORTB.RB5 =0;
  while(1)
  {
  

   if(Sys_getFrame(&Frame_System_Recive[0],'S','E',9) == 1)
   {

        if(Frame_System_Recive[1] == '0' && Frame_System_Recive[2] == 'D')
        {

                 if(Frame_System_Recive[5] == '1'){
                     PORTB.RB0 =1;
                 }
                 if(Frame_System_Recive[5] == '0'){
                     PORTB.RB0 =0;
                 }

                 if(Frame_System_Recive[6] == '1'){
                     PORTB.RB4 =1;
                 }
                 if(Frame_System_Recive[6] == '0'){
                     PORTB.RB4 =0;
                 }

                 if(Frame_System_Recive[7] == '1'){
                     PORTB.RB5 =1;
                 }
                 if(Frame_System_Recive[7] == '0'){
                     PORTB.RB5 =0;
                 }

         }
   }




  }
}


unsigned char Sys_getFrame(unsigned char *pData, unsigned char CharterStart, unsigned char CharterStop, unsigned char len){
  unsigned char Read_status;
  unsigned char Frame_status;
  unsigned char Data_val ;
  unsigned char i,k;

  Frame_status = 0;
  // lay data tu Buffer va kiem tra xem co Frame gui ve khong.
  while((RxUart1.Len > 0)&&(Frame_status == 0)){
      Read_status = Rd_buff(&RxUart1,30, &Data_val);
      // kiem tra data new
      if(Read_status == 1){
        // Shiff toan bo data sang phai 1 byte
        for( i = 0;i < len;i++){
           Shiff_frame_sys[len - i -1] = Shiff_frame_sys[len - i - 2];
        }
        // day data moi vao
        Shiff_frame_sys[0] = Data_val;
        // kiem tra Frame dung la 1 frame
            if((Shiff_frame_sys[len-1] == CharterStart) && (Shiff_frame_sys[0] == CharterStop)){
              // truyen frame len tren
              for( k = 0; k < len;k++){
                *pData = Shiff_frame_sys[len - k - 1];
                pData++;
                Shiff_frame_sys[len - k - 1] = 0;
              }
              Frame_status = 1;
            }
            else {Frame_status = 0;}
      }
  }
  // return 1 neu co Frame, 0 neu khong co Frame.
  return Frame_status;
}


unsigned char Rd_buff(RxBuffUart_T *buf_prt,unsigned char buf_size,  unsigned char *oval_prt){
  unsigned char rd_status = 0xff;
  //uint8_t new_idx = (*RxBuffUart3.Idx + *RxBuffUart3.Len)%buf_size; // loop buff
  if(buf_prt->Len > 0){
    *oval_prt = buf_prt->RxBuff[buf_prt->Idx];
    buf_prt->Idx = (buf_prt->Idx + 1)% (buf_size );
    buf_prt->Len--;
    rd_status = 1;
  }
  else{
    *oval_prt = 0;
    rd_status = 0;
  }

  return rd_status;
}

unsigned char Wr_buff(RxBuffUart_T *buf_prt,unsigned char buf_size, unsigned char val)
{
  unsigned char wr_status;
  unsigned char new_idx ;
  new_idx = (buf_prt->Idx + buf_prt->Len) % (buf_size ); // rollover buff
  if(buf_prt->Len < buf_size){
    buf_prt->RxBuff[new_idx] = val;
    buf_prt->Len++;
    wr_status = 1;
  }
  else{wr_status = 0;}

  return wr_status;
}