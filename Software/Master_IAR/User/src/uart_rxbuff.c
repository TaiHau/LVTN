/**
  ******************************************************************************
  * @file     Uart2_internet.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  


#include "uart_rxbuff.h"


/*****************************************************************
* Function name: Rd_buff 
* Description:
* Input: 
* Return:
******************************************************************/
uint8_t Rd_buff(RxBuffUart_T *buf_prt,uint8_t buf_size, __IO uint8_t *oval_prt){
  uint8_t rd_status = 0xff;
  //uint8_t new_idx = (*RxBuffUart3.Idx + *RxBuffUart3.Len)%buf_size; // loop buff
  if(buf_prt->Len > 0){
    *oval_prt = buf_prt->RxBuff[buf_prt->Idx];
    buf_prt->Idx = (buf_prt->Idx + 1)% (buf_size );
    buf_prt->Len--;
    rd_status = 1;
  }
  else{
    *oval_prt = 0;
    rd_status = 0;
  }
  
  return rd_status;
}

/*****************************************************************
* Function name: Wr_buff 
* Description:
* Input: 
* Return:
******************************************************************/
uint8_t Wr_buff(RxBuffUart_T *buf_prt,uint8_t buf_size, uint8_t val){
  uint8_t wr_status;
  uint8_t new_idx ;
  new_idx = (buf_prt->Idx + buf_prt->Len) % (buf_size ); // rollover buff
  if(buf_prt->Len < buf_size){
    buf_prt->RxBuff[new_idx] = val;
    buf_prt->Len++;
    wr_status = 1;
  }
  else{wr_status = 0;}
  
  return wr_status;
}