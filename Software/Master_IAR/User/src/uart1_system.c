/**
  ******************************************************************************
  * @file     Uart1_system.c
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/ 

/* Includes ------------------------------------------------------------------*/
#include "uart1_system.h"


/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
 RxBuffUart_T  RxUart1;

/* Private function prototypes -----------------------------------------------*/

/*******************************************************************************
* Function name: BSP_Si4463_stm8s_Init
* Description:
* Input: 
* Return:
*******************************************************************************/
void UART1_System_Init(void){
  GPIO_InitTypeDef GPIO_InitStructure;

  /* config SEC_UART1 pin output */
  GPIO_InitStructure.GPIO_Pin = SEC_UART1_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  GPIO_ResetBits(GPIOA,SEC_UART1_PIN);
  
  
  /* Configure USART1 Rx as input floating */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
 
  
  /* Configure USART1 Tx as alternate function push-pull */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
  GPIO_Init(GPIOA, &GPIO_InitStructure);
  
  
  USART_InitTypeDef USART_InitStructure;
  USART_InitStructure.USART_BaudRate = 9600;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  
  USART_DeInit(USART1);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
  USART_Init(USART1,&USART_InitStructure);
  
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
  USART_Cmd(USART1,ENABLE);
}
/*******************************************************************************
* Function name: BSP_Si4463_stm8s_Init
* Description:
* Input: 
* Return:
*******************************************************************************/
void UART1_System_Read_isr(void){
   //--------new
   Wr_buff(&RxUart1,RxBuffUART_size,(uint8_t)USART_ReceiveData(USART1));
}
//------------------------------------------------------------------------------