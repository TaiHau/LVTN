/**
  ******************************************************************************
  * @file     Uart2_internet.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

#ifndef __UART2_INTERNET_H
#define __UART2_INTERNET_H
/* Includes ------------------------------------------------------------------*/

#include "stm32f10x_usart.h"
#include "stm32f10x_gpio.h"
#include "uart_rxbuff.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/


/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

 extern RxBuffUart_T  RxUart2;
/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void UART2_Internet_Init(void);
void UART2_Internet_Read_isr(void);
void UART2_Internet_write(uint8_t *iData,uint8_t NumByte);


#endif