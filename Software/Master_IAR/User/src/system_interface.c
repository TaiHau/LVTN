/**
  ******************************************************************************
  * @file     system_interface.h
  * @author  Nguyentaihau
  * @version 
  * @date    10-9-2018
  * @brief   Uart2_internet
  ******************************************************************************
  **/  

/* Includes ------------------------------------------------------------------*/
#include "system_interface.h"
#include "delay.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern  uint8_t flag_internet_send_to_system;
extern  uint8_t flag_sys_send_to_sys;
extern  uint8_t flag_sys_send_to_internet;

static uint8_t Frame_System_Recive[9];
extern  uint8_t Frame_Internet_Send[9];
extern  uint8_t Frame_Systerm_Send[9];

static uint8_t Shiff_frame_sys[9];
static uint8_t flag_update_sys = 0;
static sys_state_t state_sys = SYS_RX_IDLE;
static sys_state_t old_state_sys = SYS_RX_IDLE;

/*******************************************************************************
* Function name: System_Init
* Description:
* Input: 
* Return:
*******************************************************************************/
void System_Init(void){
  UART1_System_Init();
  
}


sys_state_t App_Sytem_State(sys_state_t curr_state,
                            sys_state_t *old_state){
  sys_state_t state;
  *old_state = curr_state;
  switch(curr_state){
  case SYS_RX_IDLE:
    if(flag_update_sys == 1){
      state = SYS_RX_PARSER;
    }
    else state = SYS_RX_IDLE;
    break;
  case SYS_RX_PARSER:
    state = SYS_RX_IDLE;
    break;
  default:
    state = SYS_RX_IDLE;
    break;
  
  }
  return state;
}


void  App_System_Poll(void){
   flag_update_sys = Sys_getFrame(&Frame_System_Recive[0],'S','E',9);
  state_sys = App_Sytem_State(state_sys,&old_state_sys);
  if(state_sys == SYS_RX_IDLE){
    flag_update_sys = 0;
  }
  if((state_sys != old_state_sys)&&(state_sys == SYS_RX_PARSER)){
    if((Frame_System_Recive[1] == '1') && (Frame_System_Recive[2] == 'B') ){
      Frame_Systerm_Send[0] = 'S';
      Frame_Systerm_Send[1] = '0';
      Frame_Systerm_Send[2] = 'D';
      Frame_Systerm_Send[3] = '0';
      Frame_Systerm_Send[4] = '0';
      if(Frame_Systerm_Send[5] != Frame_System_Recive[5]){
        Frame_Systerm_Send[5] = Frame_System_Recive[5];
        flag_sys_send_to_sys = 1;
      }
      if(Frame_Systerm_Send[6] != Frame_System_Recive[6]){
        Frame_Systerm_Send[6] = Frame_System_Recive[6];
        flag_sys_send_to_sys = 1;
      }
      if(Frame_Systerm_Send[7] != Frame_System_Recive[7]){
        Frame_Systerm_Send[7] = Frame_System_Recive[7];
        flag_sys_send_to_sys = 1;
      }
      
      Frame_Systerm_Send[8] = 'E';
      
      
      // send status len internet.
      Frame_Internet_Send[0] = 'S';
      Frame_Internet_Send[1] = '0';
      Frame_Internet_Send[2] = 'I';
      Frame_Internet_Send[3] = '1';
      Frame_Internet_Send[4] = '0';
      if(Frame_Internet_Send[5] != Frame_System_Recive[5]){
        Frame_Internet_Send[5] = Frame_System_Recive[5];
        flag_sys_send_to_internet = 1;
      }
      if(Frame_Internet_Send[6] != Frame_System_Recive[6]){
        Frame_Internet_Send[6] = Frame_System_Recive[6];
        flag_sys_send_to_internet = 1;
      }
      if(Frame_Internet_Send[7] != Frame_System_Recive[7]){
        Frame_Internet_Send[7] = Frame_System_Recive[7];
        flag_sys_send_to_internet = 1;
      }
      
      Frame_Internet_Send[8] = 'E';
      
    }
  }
  if((flag_sys_send_to_sys == 1)||(flag_internet_send_to_system == 1)){
      // gui du lieu cho slave
      sendRS485(&Frame_Systerm_Send[0],10);
      flag_sys_send_to_sys = 0;
      flag_internet_send_to_system = 0;
  }
}

uint8_t Sys_getFrame(uint8_t *pData, uint8_t CharterStart, uint8_t CharterStop, uint8_t len){
  uint8_t Read_status;
  uint8_t Frame_status;
  uint8_t Data_val ;
  uint8_t i,k;

  Frame_status = 0;
  // lay data tu Buffer va kiem tra xem co Frame gui ve khong.
  while((RxUart1.Len > 0)&&(Frame_status == 0)){
      Read_status = Rd_buff(&RxUart1,RxBuffUART_size, &Data_val);
      // kiem tra data new
      if(Read_status == 1){
        // Shiff toan bo data sang phai 1 byte
        for( i = 0;i < len;i++){
           Shiff_frame_sys[len - i -1] = Shiff_frame_sys[len - i - 2];
        }
        // day data moi vao
        Shiff_frame_sys[0] = Data_val;
        // kiem tra Frame dung la 1 frame
            if((Shiff_frame_sys[len-1] == CharterStart) && (Shiff_frame_sys[0] == CharterStop)){
              // truyen frame len tren
              for( k = 0; k < len;k++){
                *pData = Shiff_frame_sys[len - k - 1];
                pData++;
                Shiff_frame_sys[len - k - 1] = 0;
              }
              Frame_status = 1;
            }
            else {Frame_status = 0;}
      }
  }
  // return 1 neu co Frame, 0 neu khong co Frame.
  return Frame_status;
}


void sendRS485(uint8_t *iData, uint8_t NumByte){
  GPIO_SetBits(GPIOA,SEC_UART1_PIN);
  DelayMs(100);
  for(uint8_t i = 0;i < NumByte;i++){
    USART_SendData(USART1,*iData);
    while(USART_GetFlagStatus(USART1,USART_FLAG_TXE)==RESET);
    iData++;
  }
  GPIO_ResetBits(GPIOA,SEC_UART1_PIN);
}