/*
System:
  S X X1 X2 X3X4X5X6 E
	X = 0 Master --> Slave
	X = 1 Slave --> Master
	X1 = 0; gui cho master
	X1 = 1; dia cho TB1
	X1 = 2; TB2
	X1 = 3; TB3
	X1 = 4; TB4
	X1 = 5; Slave sensor
	X1 = 6; Slave Power
	
	X2 function
	X2 = 0; Trang thai TB
	X2 = 1; SetTime on
	X2 = 2; SetTime off
	X2 = 3; Do am,Nhiet Do

Internet
  X1 X2X3X4X5
	X1: function

*/

#include "stm32f4xx_hal.h"

UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart3_rx;

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);

// UART2 Internet
uint8_t dem_internet = 0,flag_update_internet=0,tem_dat_receive_internet=0;
uint8_t dat_receive_internet[9]="00000000" ;
uint8_t dat_sen_internet[9];

//UART3 System
uint8_t dem_system=0,flag_update_system=0,tem_dat_system=0;
uint8_t dat_receive_system[9]="000000000";
uint8_t dat_sen_system[9];

// Khai bao ham
//void sendRS485(void);
//void sendRS485toSystem(void);
void sendRS485(uint8_t *iData, uint8_t NumByte);
void sendInternet(uint8_t *iData_prt, uint8_t NumByte);

int main(void)
{


  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();

	HAL_UART_Receive_DMA(&huart2,&tem_dat_receive_internet,1);
	HAL_UART_Receive_DMA(&huart3,&tem_dat_system,1);
	
	dat_sen_system[0] = 'S';
	dat_sen_system[1] = '0';
	dat_sen_system[2] = '0';
	dat_sen_system[3] = '0';
	dat_sen_system[4] = '0';
	dat_sen_system[5] = '0';
	dat_sen_system[6] = '0';
	dat_sen_system[7] = '0';
	dat_sen_system[8] = 'E';
	
	dat_sen_internet[0] = 'S';
	dat_sen_internet[1] = '0';
	dat_sen_internet[2] = '0';
	dat_sen_internet[3] = '0';
	dat_sen_internet[4] = '0';
	dat_sen_internet[5] = '0';
	dat_sen_internet[6] = '0';
	dat_sen_internet[7] = '0';
	dat_sen_internet[8] = 'E';
  while (1)
  {
		//sendInternet(&dat_sen_internet[0],9);
		//HAL_Delay(3000);
		if(flag_update_internet == 1){   //nhan duoc du lieu tu internet, xu li va gui cho slave
			flag_update_internet = 0;
			if(dat_receive_internet[1] == '1' && dat_receive_internet[2] == 'I'){
				if(dat_receive_internet[3] == '1'){
					dat_sen_system[0] = 'S';
					dat_sen_system[8] = 'E';
					dat_sen_system[1] = '0';
					dat_sen_system[2] = 'D';
					dat_sen_system[5] = dat_receive_internet[5];
					dat_sen_system[6] = dat_receive_internet[6];
					dat_sen_system[7] = dat_receive_internet[7];
					
					sendRS485(&dat_sen_system[0],9);																	// Gui du lieu cho slave device.
				}
			}
		}
		
		
		if(flag_update_system == 1){
			flag_update_system = 0;
			if(dat_receive_system[1] == '1' && dat_receive_system[2] == 'B'){											// Slave gui cho master
				dat_sen_system[1] = '0';
							dat_sen_system[0] = 'S';
							dat_sen_system[8] = 'E';
							dat_sen_system[1] = '0';
							dat_sen_system[2] = 'D';
							dat_sen_system[5] = dat_receive_system[5];
							dat_sen_system[6] = dat_receive_system[6];
							dat_sen_system[7] = dat_receive_system[7];
				
							sendRS485(&dat_sen_system[0],9);
				
							dat_sen_internet[1] = '0';
							dat_sen_internet[2] = 'I';
							dat_sen_internet[3] = '1';
							dat_sen_internet[4] = '0';
							dat_sen_internet[5] = dat_receive_system[5];
							dat_sen_internet[6] = dat_receive_system[6];
							dat_sen_internet[7] = dat_receive_system[7];
	
							sendInternet(&dat_sen_internet[0],9);
																								
			}
		}
  }
}
// chuong trinh ngat UART
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	// nhan du lieu tu internet
	if(huart->Instance==huart2.Instance){
		HAL_UART_Receive_IT(&huart2,&tem_dat_receive_internet,1);
		if(tem_dat_receive_internet == 'S'){
			dem_internet = 0;
			dat_receive_internet[dem_internet] = tem_dat_receive_internet;
			dem_internet++;
		}
		if(tem_dat_receive_internet != 'S' && tem_dat_receive_internet != 'E' && tem_dat_receive_internet != 0x0A && tem_dat_receive_internet != 0x0D){
			dat_receive_internet[dem_internet] = tem_dat_receive_internet;
			dem_internet++;
		}
		if(tem_dat_receive_internet == 'E'){
			dat_receive_internet[dem_internet] = tem_dat_receive_internet;
			dem_internet = 0;
			flag_update_internet=1;
		}
	}
	// nhan du lieu tu system
	if(huart->Instance==huart3.Instance){
		if(tem_dat_system == 'S'){
			dem_system = 0;
			dat_receive_system[dem_system] = tem_dat_system;
			dem_system++;
		}
		if(tem_dat_system != 'S' && tem_dat_system != 'E'){
			dat_receive_system[dem_system] = tem_dat_system;
			dem_system++;
		}
		if(tem_dat_system == 'E'){
			dat_receive_system[dem_system] = tem_dat_system;
			dem_system = 0;
			flag_update_system = 1;
		}
	}
}
// Ham gui du lieu den Slave

void sendRS485(uint8_t *iData, uint8_t NumByte){
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_SET);
	HAL_Delay(100);
	for (uint8_t i = 0; i<NumByte;i++){
		HAL_UART_Transmit_IT(&huart3,iData,1);
		HAL_Delay(5);
		iData++;
	}
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_RESET);
}
/*
void sendRS485toSystem(void){

	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_SET);
	HAL_Delay(100);
	HAL_UART_Transmit_IT(&huart3,dat_sen_system,9);

	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOD,GPIO_PIN_12,GPIO_PIN_RESET);

}
*/
// Ham gui du lieu len Internet
void sendInternet(uint8_t *iData_prt, uint8_t NumByte){
	for(uint8_t i = 0; i < NumByte; i++){
		HAL_UART_Transmit_IT(&huart2,iData_prt,1);
		HAL_Delay(1);
		iData_prt++;
	}
	
}
/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 336;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  HAL_RCC_OscConfig(&RCC_OscInitStruct);

  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;
  HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);

  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USART2 init function */
void MX_USART2_UART_Init(void)
{

  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  HAL_UART_Init(&huart2);

}

/* USART3 init function */
void MX_USART3_UART_Init(void)
{

  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  HAL_UART_Init(&huart3);

}

/** 
  * Enable DMA controller clock
  */
void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);
	
	/*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);

  /*Configure GPIO pins : PD12 PD13 PD14 PD15 */
  GPIO_InitStruct.Pin = GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : PB5 */
  GPIO_InitStruct.Pin = GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
